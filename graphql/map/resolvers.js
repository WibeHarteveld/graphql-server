import uuid from 'uuid';

const maps = [
	{
		id: '01232345',
		name: 'main',
		position: {
			lng: 4.775,
			lat: 52.1318,
		},
		zoom: 9.21,
		isNavigating: false,
		isWatchingUserPosition: false,
		userPositionWatchId: null,
		userLocationVisible: true,
		navZoom: 16,
		isFollowing: true,
		isRouteShown: false,
	},
	// {
	// 	id: "1",
	// 	name: 'other',
	// 	position: {
	// 		lng: 4.775,
	// 		lat: 52.1318,
	// 	},
	// 	zoom: 9.21,
	// }
]

const userPosition = {
	lng: 4.775,
	lat: 52.1318
};

// addChannel: (root, args) => {
// 	const newChannel = {
// 		id: uuid(),
// 		name: args.name,
// 	}
// 	channels.push(newChannel);
// 	return newChannel;
// }

export const resolvers = {
	Query: {
		maps: (root, args) => {
			if (args.name) {
				return maps.filter((obj) => obj.name === args.name);
			}
			return maps;
		},
		map: (root, args) => {
			if (args.name) {
				return maps.find((obj) => obj.name === args.name);
			}
		},
		userPosition: () => userPosition,
	},

	Mutation: {
		// toggleNavigation: (root, args) => {
		// 	return {
		// 		id: args.id,
		// 		isNavigating: !args.isNavigating,
		// 	};
		// }
		updateMapPosition: (root, args) => {
			const newMap = {
				position: {
					lng: args.lng,
					lat: args.lat,
				},
				zoom: args.zoom
			}
			const i = maps.findIndex((obj) => obj.id === args.id);
			maps[i] = Object.assign(maps[i], newMap);
			return maps[i];
		},
		updateUserPosition: (root, args) => {
			const newUserPosition = {
				lng: args.lng,
				lat: args.lat
			}

			userPosition.lng = newUserPosition.lng;
			userPosition.lat = newUserPosition.lat;

			return userPosition;
		}
	},

	Map: {
		__resolveType(obj, context, info){
		  if(obj.hasOwnProperty('isNavigating')) {
			return 'MainMap';
		  }

		  return 'OtherMap';
		},
	}
}
