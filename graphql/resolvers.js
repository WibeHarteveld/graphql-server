import { merge } from 'lodash';
import { resolvers as channelResolvers } from './channel/resolvers';
import { resolvers as mapResolvers } from './map/resolvers';
import { resolvers as userResolvers } from './user/resolvers';

export default merge(
	{},
	channelResolvers,
	mapResolvers,
	userResolvers
);
