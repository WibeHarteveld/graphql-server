import uuid from 'uuid';

const channels = [
	{
		id: uuid(),
		name: 'soccer',
	}, {
		id: uuid(),
		name: 'baseball',
	}
];

export const resolvers = {
	Query: {
		channels: () => channels,
	},
	Mutation: {
		addChannel: (root, args, context) => {
			if (!context.user || !context.user.roles.includes('admin')) return null;

			const newChannel = {
				id: args.id,
				name: args.name,
			}

			channels.push(newChannel);

			return newChannel;
		},
		removeChannel: (root, args) => {
			const index = channels.findIndex(x => x.id === args.id);

			const curChannel = {
				id: args.id,
				name: channels[index].name,
			}

			// const filteredChannels = channels.filter(
			// 	item => item.id !== args.id
			// )

			// channels.filter(
			// 	item => item.id !== args.id
			// );

			channels.splice(index, 1);

			return curChannel;
		},
		changeChannelName: (root, args) => {
			const index = channels.findIndex(x => x.id === args.id);

			channels[index].name = args.name;

			return channels[index];
		}
	}
};
