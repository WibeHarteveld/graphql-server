import passport from 'passport';
import User from '../../models/user.server.model';

export const resolvers = {
	Query: {
		users: (_, args, context) => {
			if (!context.user || !context.user.roles.includes('admin')) return [];
 			return context.models.User.getAll();
		},
		user: (_, args, context) => {
			// if (!context.user || !context.user.roles.includes('admin')) return null;
			return context.models.User.getById(args.id);
		},
	},

	Mutation: {
		signup: (_, { name, password }) => {
			User.register(new User({ name }), password, function(err, account) {
				if (err) {
					return 'I do not know'
				}

				// passport.authenticate('local')(req, res, function () {
				// 	res.redirect('/');
				// });
			});
		},
		login: (_, { name, password }, context) => {
			return context.models.User.getById(args.name);

			if (context.user) {
				return { id, name, email };
			}

			passport.authenticate()(name, password);
		}
	}
}

// Mutation: {
// 	register: (root, args) => {
// 		const newMap = {
// 			position: {
// 				lng: args.lng,
// 				lat: args.lat,
// 			},
// 			zoom: args.zoom
// 		}
// 		const i = maps.findIndex((obj) => obj.id === args.id);
// 		maps[i] = Object.assign(maps[i], newMap);

// 		return maps[i];
// 	},
// }
