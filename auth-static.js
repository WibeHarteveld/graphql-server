import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
// import { Strategy as GitHubStrategy } from 'passport-github2';
// import User from './models/user';

passport.use(new LocalStrategy(
	function(username, password, done) {
		User.findOne({ username: username }, function (err, user) {
		if (err) { return done(err); }
		if (!user) { return done(null, false); }
		if (!user.verifyPassword(password)) { return done(null, false); }
			return done(null, user);
		});
	}
));

// passport.serializeUser(function(user, done) {
//   done(null, user.id);
// });

// passport.deserializeUser(function(id, done) {
// 	user.findById(id, function(err, user) {
// 		done(err, user);
// 	});
// });

// passport.serializeUser(User.serializeUser());
// passport.deserializeUser(User.deserializeUser());
