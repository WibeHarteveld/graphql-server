import passport from 'passport';
// import { Strategy as LocalStrategy } from 'passport-local';
// import { Strategy as GitHubStrategy } from 'passport-github2';
import User from './models/user.server.model';

// passport.use(new LocalStrategy(User.authenticate()));
passport.use(User.createStrategy());

// passport.use(new LocalStrategy(
// 	function(username, password, done) {
// 		User.findOne({ username: username }, function (err, user) {
// 			if (err) { return done(err); }
// 			if (!user) { return done(null, false); }
// 			if (!user.verifyPassword(password)) { return done(null, false); }
// 			return done(null, user);
// 	  });
// 	}
// ));

// passport.use(new GitHubStrategy({
//     clientID: GITHUB_CLIENT_ID,
//     clientSecret: GITHUB_CLIENT_SECRET,
//     callbackURL: "http://127.0.0.1:3000/auth/github/callback"
//   },
//   function(accessToken, refreshToken, profile, done) {
//     User.findOrCreate({ githubId: profile.id }, function (err, user) {
//       return done(err, user);
//     });
//   }
// ));

// passport.serializeUser(function(user, done) {
//   done(null, user.id);
// });

// passport.deserializeUser(function(id, done) {
// 	user.findById(id, function(err, user) {
// 		done(err, user);
// 	});
// });

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
