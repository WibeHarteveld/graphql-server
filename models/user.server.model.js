// import uuid from 'uuid'
// import bcrypt from 'bcryptjs'
// import jwt from 'jsonwebtoken'
import mongoose, { Schema } from 'mongoose';
import passportLocalMongoose from 'passport-local-mongoose';

// const UserSchema = new Schema({
// 	username: String,
// 	email: String,
//  password: String
// });

const UserSchema = new Schema({
	active: Boolean
});

// UserSchema.plugin(passportLocalMongoose, {
// 	// Needed to set usernameUnique to false to avoid a mongodb index on the username column!
// 	usernameUnique: false,

// 	findByUsername: function(model, queryParameters) {
// 	  // Add additional query parameter - AND condition - active: true
// 	  queryParameters.active = true;
// 	  return model.findOne(queryParameters);
// 	}
// });

UserSchema.plugin(passportLocalMongoose);

const user = [
	{
		id: '12',
		name: 'Wibe',
		roles: ['user', 'admin']
	},
	{
		id: '13',
		name: 'Gert',
		roles: ['user']
	},
	{
		id: '14',
		name: 'Mariska',
		roles: ['user']
	}
];

function getAll() {
	return user;
}

function getById(id) {
	return user.find((obj) => obj.id === id);
}

function getByGroupId(id) {
	return [];
}

// const APP_SECRET = 'GraphQL-is-aw3some'

// function getUserid(context) {
// 	const Authorization = context.request.get('Authorization')

// 	if (Authorization) {
// 		const token = Authorization.replace('Bearer ', '')
// 		const { userId } = jwt.verify(token, APP_SECRET)
// 		return userId
// 	}

// 	throw new Error('Not authenticated')
// }

// function post(parent, args, context, info) {
// 	const userId = getUserId(context)
// 	return 'posted'
// }

// async function signup(parent, args, context, info) {
// 	const password = await bcrypt.hash(args.password, 10)
// 	const user = await context.db.mutation.createUser({
// 	  data: { ...args, password },
// 	}, `{ id }`)

// 	const token = jwt.sign({ userId: user.id }, APP_SECRET)

// 	return {
// 	  token,
// 	  user,
// 	}
// }

// async function login(parent, args, context, info) {
// 	const user = await context.db.query.user({ where: { email: args.email } }, `{ id password }`)
// 	if (!user) {
// 	  throw new Error('No such user found')
// 	}

// 	const valid = await bcrypt.compare(args.password, user.password)
// 	if (!valid) {
// 	  throw new Error('Invalid password')
// 	}

// 	return {
// 	  token: jwt.sign({ userId: user.id }, APP_SECRET),
// 	  user,
// 	}
// }

export const generateUserModal = ({ user }) => ({
	getAll,
	getById,
	getByGroupId
})

export default mongoose.model('User', UserSchema);

