# Graph server
`npm start`

Sources:
- [Navigating your transition to GraphQL](https://dev-blog.apollodata.com/navigating-your-transition-to-graphql-28a4dfa3acfb)
- [Tutorial: How to build a GraphQL server](https://dev-blog.apollodata.com/tutorial-building-a-graphql-server-cddaa023c035)

Tools:
- [Recompose](https://github.com/acdlite/recompose): A React utility belt for function components and higher-order components

## Guides and best practices
- [GraphQL Shorthand Notation Cheat Sheet](https://github.com/sogko/graphql-schema-language-cheat-sheet)
- [GraphQL Best Practices](https://graphql.org/learn/best-practices/)
- [https://www.graphql.com/guides/](https://www.graphql.com/guides/)
- [dataloader | batch queries](https://github.com/facebook/dataloader)
- [http-link-dataloader](https://github.com/prismagraphql/http-link-dataloader)
- [GraphQL Database design patterns](https://stackoverflow.com/questions/43076317/graphql-database-design-patterns)
