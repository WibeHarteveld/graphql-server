import path from 'path';
import express from 'express';
import session from 'express-session';
import fs from 'fs';
import cors from 'cors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import dotenv from 'dotenv';
import { ApolloServer, gql } from 'apollo-server-express';
import mongoose from 'mongoose';
import compression from 'compression';
import uuid from 'uuid';
import passport from 'passport';

import resolvers from './graphql/resolvers';
import { generateUserModal } from './models/user.server.model';
import './auth.js';
import * as userCtrl from './controllers/user.server.controller';
// import './auth-static.js'

dotenv.config();

const {
	MONGO_DB_USERNAME,
	MONGO_DB_PASSWORD,
	APOLLO_ENGINE_KEY } = process.env;

const PORT = 4000;
const typeDefs = gql`
	${fs.readFileSync(__dirname.concat('/graphql/base.graphql'), 'utf8')}
	${fs.readFileSync(__dirname.concat('/graphql/channel/schema.graphql'), 'utf8')}
	${fs.readFileSync(__dirname.concat('/graphql/map/schema.graphql'), 'utf8')}
	${fs.readFileSync(__dirname.concat('/graphql/user/schema.graphql'), 'utf8')}
`;

const shouldCompress = (req, res) => {
	if (req.headers['x-no-compression']) {
		// don't compress responses with this request header
		return false;
	}

	// fallback to standard filter function
	return compression.filter(req, res);
};

// Connect to database
mongoose.connect(
	`mongodb+srv://${MONGO_DB_USERNAME}:${MONGO_DB_PASSWORD}@cluster0-xzk5e.mongodb.net/test?retryWrites=true`,
	{ useNewUrlParser: true });

mongoose.set('useCreateIndex', true);

const app = express();

app.use(compression({filter: shouldCompress}));
app.use(bodyParser.urlencoded({ extended: false }) );
app.use('*', cors({ origin: 'http://localhost:3000' }));
app.use(cookieParser());
app.use(session({
	secret: 'keyboard cat',
	resave: false,
  	saveUninitialized: false
}));

app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());

// app.post('/graphql', (req, res) => {
// 	graphql(schema, req.body, { user: req.user })
// 	.then((data) => {
// 		res.send(JSON.stringify(data));
// 	});
// });

// login route for passport
// app.get("/",function(req, res) {
// 	res.send("Hello world");
// });

app.route('/login')
	.get(function(req, res) {
		res.sendFile(path.join(__dirname, 'public/login.html'));
	})
	// .post(userCtrl.authenticate);

	.post(passport.authenticate('local', {
		successRedirect: '/admin',
		failureRedirect: '/login',
		// session: false
	}));

app.get('/logout', function(req, res) {
	req.logout();
	req.session.destroy();
	res.clearCookie('connect.sid');
	res.redirect('/');
});

app.get("/admin",function(req, res) {
	if (req.user) {
		res.sendFile(path.join(__dirname, 'public/admin.html'));
	} else {
		res.send("Unauthorized, incorrect username or password");
	}
});

app.route('/register')
	.post(function(req, res) {
		userCtrl.register(req, res);
		res.send("register successful");
	})
	.get(function(req, res) {
		res.send("register successful");
	});

function getUser() {
	return {
		id: '12',
		name: 'Wibe',
		roles: ['user', 'admin']
	}
}

// Start the app
app.listen({
	port: PORT,
}, () => {
	console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`)
});

const server = new ApolloServer({
	typeDefs,
	resolvers,
	engine: {
		apiKey: APOLLO_ENGINE_KEY
	},
	context: ({ req }) => {
		// get the user token from the headers
		const token = req.headers.authorization || '';

		// try to retrieve a user with the token
		const user = getUser(token);

		// optionally block the user
		// we could also check user roles/permissions here
		// if (!user) throw new AuthorizationError('you must be logged in');

		// add the user to the context
		return {
			user,
			models: {
				User: generateUserModal({ user })
			}
		 };
	}
});

server.applyMiddleware({ app });
