import User from '../models/user.server.model';

export const register = function(req, res) {
	// const entry = new User({
	// 	username: req.body.username,
	// 	email: req.body.email,
	// 	password: req.body.password
	// });

	// entry.save();

	User.register(
		{ username: req.body.username },
		req.body.password,
		function(err, user) {
			if (err) { console.log('Error while user register!', err); }

			console.log('User registered!');
		}
	);
}

export const authenticate = function(req, res, next) {
	// const entry = new User({
	// 	username: req.body.username,
	// 	email: req.body.email,
	// 	password: req.body.password
	// });

	// entry.save();

	const authenticate = User.authenticate();

	authenticate(req.body.username, req.body.password, function(err, user) {
		if (err) { return next(err); }

		if (!user) { return res.redirect('/login'); }

		req.logIn(user, function(err) {
		  if (err) { return next(err); }
		  return res.redirect('/admin');
		});
	});

	// passport.authenticate('local', function(err, user, info) {
	// 	if (err) { return next(err); }
	// 	if (!user) { return res.redirect('/login'); }
	// 	req.logIn(user, function(err) {
	// 	  if (err) { return next(err); }
	// 	  return res.redirect('/users/' + user.username);
	// 	});
	//   })(req, res, next);
}
